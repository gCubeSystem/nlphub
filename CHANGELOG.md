This project adheres to [Semantic Versioning](https://semver.org/spec/v2.0.0.html).

# Changelog for "nlphub"


## [v1.2.0-SNAPSHOT] - 2022-05-20

### Features

- Updated to StorageHub 2



## [v1.1.0] - 2021-09-08

### Features

- Updated to StorageHub [#21970]



## [v1.0.1] - 2019-06-18

### Features

- Added Google Analytics support [#17015]



## [v1.0.0] - 2019-02-25

### Features

- First Release


