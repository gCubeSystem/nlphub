<%@ page import="org.gcube.data.analysis.nlphub.shared.Constants"  %>
<!DOCTYPE html>
<html>
<head>
<!-- input=http%3A%2F%2Fws1-clarind.esc.rzg.mpg.de%2Fdrop-off%2Fstorage%2F1513257926038.txt&lang=en&analysis=const-parsing -->
<meta charset="utf-8" />
<meta name="viewport" content="width=device-width,initial-scale=1" />
<meta http-equiv="cache-control" content="no-cache, must-revalidate, post-check=0, pre-check=0" />
<meta http-equiv="cache-control" content="max-age=0" />
<meta http-equiv="expires" content="0" />
<meta http-equiv="expires" content="Tue, 01 Jan 1980 1:00:00 GMT" />
<meta http-equiv="pragma" content="no-cache" />
<title>NLPHub</title>
<link href="https://fonts.googleapis.com/icon?family=Material+Icons"
	rel="stylesheet" />
<!--Import materialize.css-->
<link type="text/css" rel="stylesheet" href="css/materialize.min.css"
	media="screen,projection" />
<link type="text/css" rel="stylesheet" href="css/custom.css" />
<!-- jQuery library -->
<script
	src="https://ajax.googleapis.com/ajax/libs/jquery/3.2.1/jquery.min.js"></script>
<script type="text/javascript" src="js/materialize.min.js"></script>
<script type="text/javascript" src="js/jquery.simple.websocket.min.js"></script>
<script type="text/javascript" src="js/jquery.uploadfile.min.js"></script>
<script type="text/javascript" src="js/main.js"></script>
<script type="text/javascript" src="js/merge.js"></script>
<script type="text/javascript">
     console.log('NLPHub start');
	 var activePage = "Named Entity Recognition";
	 var inputFile = '<%= ((request.getParameter(Constants.INPUT_FILE_PARAMETER) == null) ? "" :  "" + request.getParameter(Constants.INPUT_FILE_PARAMETER)) %>';
	 <% 
	 String requestToken=request.getParameter(Constants.TOKEN_PARAMETER);
	 if( requestToken == null||requestToken.isEmpty()) {
	 %>
	 var gCubeToken = "d35c72d3-f6b5-4363-afbe-8e330ef9a913-843339462";
	 <% } else { %>
	 var gCubeToken = '<%=requestToken%>';
	 
	 <% } %> 
	 var scheme = '<%=request.getHeader("x-forwarded-proto")%>';
	 console.log('Protocol: '+scheme);
	 var contextPath = '<%= request.getContextPath() %>';
</script>
<!-- Global site tag (gtag.js) - Google Analytics -->
<script async src="https://www.googletagmanager.com/gtag/js?id=UA-2731621-28"></script>
<script>
  window.dataLayer = window.dataLayer || [];
  function gtag(){dataLayer.push(arguments);}
  gtag('js', new Date());

  gtag('config', 'UA-2731621-28');
</script>

</head>
<body style="padding: 0 15px;">

	<div class="logo">
		<img id="logo-image" src="img/nlphub-logo-3.png">
	</div>

	<!-- the "main-div" contains all tabs and contents -->
	<div class="main-div">
		<!--  tabs div: each tab must refer a page div -->
		<div class="col s12">
			<ul class="tabs">
				<li class="tab col s3"><a href="#ner" onclick="activePage='Named Entity Recognition'">NER</a></li>
				<!--  <li class="tab col s3"><a href="#other" onclick="activePage='Other'">other</a></li>-->
			</ul>
		</div>

		<!--  "ner" div: contains the name entity recognizer interface -->
		<div id="ner">
			<div id="ner-ui">
				<p class="flow-text">Named Entity Recognition</p>
				<fieldset>
					<legend>Language selection</legend>
					<div class="row">
						<div class="clearfix">
							<div class="column half-width">
								<p class="margin-3 text-align-right"></p>
							</div>
							<div class="column half-width">
								<select id="language-select" class="margin-3 align-left">
								</select>
							</div>
						</div>
					</div>
				</fieldset>

				<fieldset>
					<legend>Input text</legend>
					<div class="row">
						<div class="clearfix">
							<div class="column margin-right-10px">
								<p>Drag a .TXT file on the Upload box, or select a file from your PC, or paste a text.</p>
							</div>
							<div class="column margin-left-10px">
								<div class="centered full-width" id="fu-container">
									<div class="waves-effect waves-light darken-1"
										id="upload-button">Upload text file</div>
								</div>
							</div>
						</div>
						<div class="input-field col s12">Write or paste a text in the text area (<span style="color:red;">max 4000 characters</span>)
							<textarea maxlength="4000" id="input-textarea" class="my-textarea" rows="8"
								placeholder="paste your text here"></textarea>
						</div>
					</div>
				</fieldset>


				<fieldset>
					<legend>Annotations (deselect those you don't want to be reported)</legend>
					<div class="vscrollable">
						<table id="annotations-table">
						</table>
					</div>
				</fieldset>

				<!--  this is the "execute button" -->
				<div style="text-align: center; padding: 5px;">
					<a id="execute-button">Analyse</a>
				</div>
			</div>
			<!--  this is the main result container  -->
			<div id="ner-result-container">
				 <div id="result-header" class="header-side">
					<div class="left-side-half" id="result-header-left"></div>
					<div class="left-side-half" id="result-header-right"></div>
				</div>
				<div></div>
				<div class="left-side" id="result-text-div"></div>
				<div class="right-side">
					<ul class="collapsible" data-collapsible="accordion" id="ner-result-accordion">
						<li>
							<div class="collapsible-header waves-effect waves-teal">Algorithms</div>
							<div class="collapsible-body" id="algorithm-params-div"></div>
						</li>

						<li>
							<div class="collapsible-header waves-effect waves-teal">Annotations</div>
							<div class="collapsible-body" id="result-params-div"></div>
						</li>
					</ul>
				</div>
				<div style="text-align: left;">
					<div class="green-button float-right-div" id="back-ner-ui-button">back</div>
				</div>
			</div>
		</div>
		<!--  end "#ner" -->

		<div id="#other"></div>

		<!-- this section is used only for the operation progress indicator -->
		<div class="hidden-div progress-circular-div">
			<div class="preloader-wrapper big active">
				<div class="spinner-layer spinner-blue-only">
					<div class="circle-clipper left">
						<div class="circle"></div>
					</div>
					<div class="gap-patch">
						<div class="circle"></div>
					</div>
					<div class="circle-clipper right">
						<div class="circle"></div>
					</div>
				</div>
			</div>
			<div id="progress-caption">In progress</div>
		</div>
		
		<div class="popup-text">
			<span class="close">&times;</span>
			<div class="popup-text-content"></div>
		</div> 


	</div>
	<!-- end "#main-div" -->
</body>
</html>