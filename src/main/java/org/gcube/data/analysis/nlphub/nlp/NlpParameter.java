package org.gcube.data.analysis.nlphub.nlp;

public class NlpParameter {
	public static String ANNOTATION_LIST = "annotations";
	public static int INPUT_FILE = 0;
	public static int INPUT_ANNOTATIONS = 1;
	public static int INPUT_LANGUAGE = 2;
	private String name, description;
	private Object value;
	private int objectType;
	
	public NlpParameter() {
	}
	
	public NlpParameter(String name, String description, Object value, int objectType) {
		this.name = name;
		this.description = description;
		this.value = value;
		this.objectType = objectType;
	}

	public String getName() {
		return name;
	}

	public String getDescription() {
		return description;
	}

	public Object getValue() {
		return value;
	}

	public int getObjectType() {
		return objectType;
	}

	public void setName(String name) {
		this.name = name;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public void setValue(Object value) {
		this.value = value;
	}

	public void setObjectType(int objectType) {
		this.objectType = objectType;
	}
}
