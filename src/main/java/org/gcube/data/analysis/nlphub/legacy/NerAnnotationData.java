package org.gcube.data.analysis.nlphub.legacy;

import java.util.ArrayList;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.google.gson.JsonArray;
import com.google.gson.JsonObject;

public class NerAnnotationData {
	private static final Logger logger = LoggerFactory.getLogger(NerAnnotationData.class);

	private String name;
	private ArrayList<NerEntity> nerEntities;
	
	
	public NerAnnotationData(String name) {
		logger.debug("NerAnnotationData: "+name);
		this.name = name;
		nerEntities = new ArrayList<>();
	}
	
	
	public void addNerEntity(NerEntity entity) {
		nerEntities.add(entity);
	}

	
	public JsonObject toJson() {
		JsonObject json = new JsonObject();
		JsonArray entities = new JsonArray();
		for(NerEntity e : nerEntities) {
			entities.add(e.toJson());
		}
		json.add(name,entities);
		return json;
	}
	
	
	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public ArrayList<NerEntity> getNerEntities() {
		return nerEntities;
	}

	public void setNerEntities(ArrayList<NerEntity> nerEntities) {
		this.nerEntities = nerEntities;
	}
	
	/*
	public static void main(String[] args) {
		NerEntity ne1 = new NerEntity(11, 22);
		ne1.addProperty("type", "scalar");
		ne1.addProperty("unit", "hours");
		
		NerEntity ne2 = new NerEntity(33, 44);
		ne2.addProperty("type", "scalar");
		ne2.addProperty("unit", "minutes");
		
		NerAnnotationData nad = new NerAnnotationData("Measure");
		nad.addNerEntity(ne1);
		nad.addNerEntity(ne2);
		
		System.out.println(nad.toJson().toString());
	}*/

		
}
