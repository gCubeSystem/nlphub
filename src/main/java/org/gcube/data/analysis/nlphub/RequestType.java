package org.gcube.data.analysis.nlphub;


public enum RequestType {
	DataminerService("DataminerService");

	private final String id;

	private RequestType(final String id) {
		this.id = id;
	}
	
	public static RequestType getRequestTypeFromString(String res){
		if(res==null||res.isEmpty()){
			return null;
		}
		for(RequestType r:RequestType.values()){
			if(r.id.compareToIgnoreCase(res)==0){
				return r;
			}
		}
		return null;
	}
	
	
	@Override
	public String toString() {
		return id;
	}
	
	
	

}
