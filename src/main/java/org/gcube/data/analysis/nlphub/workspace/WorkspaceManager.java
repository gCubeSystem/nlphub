package org.gcube.data.analysis.nlphub.workspace;

import java.io.ByteArrayInputStream;
import java.io.InputStream;
import java.net.URL;

import org.gcube.common.storagehub.client.dsl.FileContainer;
import org.gcube.common.storagehub.client.dsl.FolderContainer;
import org.gcube.common.storagehub.client.dsl.StorageHubClient;
import org.gcube.common.storagehub.model.exceptions.StorageHubException;
import org.gcube.data.analysis.nlphub.legacy.NlpHubException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class WorkspaceManager {
	private static final Logger logger = LoggerFactory.getLogger(WorkspaceManager.class);

	public String uploadFile(byte[] in, String name, String description) throws NlpHubException {
		if (name == null || name.isEmpty()) {
			String error = "Error uploading the file, the filename is invalid: Null";
			logger.error(error);
			throw new NlpHubException(error);
		}

		StorageHubClient shc = new StorageHubClient();
		FolderContainer rootContainer = shc.getWSRoot();
		InputStream inputStream = new ByteArrayInputStream(in);

		FileContainer fileContainer;
		try {
			fileContainer = rootContainer.uploadFile(inputStream, name, description);
		} catch (StorageHubException e) {
			String error = "Error uploading the file " + name + " : " + e.getLocalizedMessage();
			logger.error(error, e);
			throw new NlpHubException(error, e);
		} 

		String itemId = fileContainer.get().getId();
		logger.debug("Item id uploaded: " + itemId);

		URL url;
		try {
			url = shc.open(itemId).asFile().getPublicLink();
		} catch (StorageHubException e) {
			String error = "Error retrieving the public link for the file " + name + " : " + e.getLocalizedMessage();
			logger.error(error, e);
			throw new NlpHubException(error, e);
		}
		logger.debug("Item public link: " + url);

		return url.toString();

	}
}
