package org.gcube.data.analysis.nlphub.legacy;

import java.util.HashMap;
import java.util.Set;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.google.gson.JsonArray;
import com.google.gson.JsonObject;


public class NerEntity {
	private static final Logger logger = LoggerFactory.getLogger(NerEntity.class);

	private int startIndex, endIndex;
	private HashMap<String, String> properties = null;
	
	/**
	 * Class constructor
	 * @param startIndex the start index of the matching annotation 
	 * @param endIndex the end index of the matching annotation
	 */
	public NerEntity(int startIndex, int endIndex) {
		logger.debug("NerEntity: [startIndex="+startIndex+", endIndex="+endIndex+"]");
		this.startIndex = startIndex;
		this.endIndex = endIndex;
		properties = new HashMap<>();
	}
	
	/**
	 * Add an additional property (property is a couple {name, value})
	 * @param name name 
	 * @param value value
	 */
	public void addProperty(String name, String value) {
		properties.put(name, value);
	}

	/**
	 * Build a proper JsonObject 
	 * @return JsonObject
	 */
	public JsonObject toJson() {
		JsonObject json = new JsonObject();
		
		// build the "indices" array
		JsonArray indices = new JsonArray();
		indices.add(startIndex);
		indices.add(endIndex);
		json.add("indices", indices);
		
		// add additional properties (if any)
		Set<String> names = properties.keySet();
		for(String n : names) {
			json.addProperty(n, properties.get(n));
		}
		
		return json;
	}
	
	public int getStartIndex() {
		return startIndex;
	}

	public void setStartIndex(int startIndex) {
		this.startIndex = startIndex;
	}

	public int getEndIndex() {
		return endIndex;
	}

	public void setEndIndex(int endIndex) {
		this.endIndex = endIndex;
	}

	public HashMap<String, String> getProperties() {
		return properties;
	}

	public void setProperties(HashMap<String, String> properties) {
		this.properties = properties;
	}
	
	/*
	public static void main(String[] args) {
		NerEntity ne = new NerEntity(11, 22);
		ne.addProperty("type", "scalar");
		ne.addProperty("unit", "hour");
		System.out.println(ne.toJson().toString());
	}*/
	
}
