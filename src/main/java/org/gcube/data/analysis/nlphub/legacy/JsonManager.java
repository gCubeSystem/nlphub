package org.gcube.data.analysis.nlphub.legacy;


import java.io.Reader;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.google.gson.JsonArray;
import com.google.gson.JsonObject;
import com.google.gson.JsonParser;

public class JsonManager {
	private static final Logger logger = LoggerFactory.getLogger(JsonManager.class);

	
	public static String TEXT = "text";
	public static String ANNOTATIONS = "annotations";
	public static String LANGUAGE = "language";
	public static String ALGORITHM = "algorithm";
	public static String ENTITIES = "entities";
	public static String INDICES = "indices";
	public static String RESULT = "result";
	public static String RESPONSE = "response";
	public static String OK = "Ok";
	public static String ERROR = "Error";
	public static String MESSAGE = "message";

	private JsonObject jsonObjectRoot = null;
	//private JsonParser jsonParser = null;

	public JsonManager() {
		logger.debug("JsonManager");
		jsonObjectRoot = new JsonObject();
	}

	public JsonManager(String s) {
		jsonObjectRoot = (JsonObject) new JsonParser().parse(s);
	}

	public JsonManager(Reader r) {
		jsonObjectRoot = (JsonObject) new JsonParser().parse(r);
	}

	public String getJsonAsString() {
		return jsonObjectRoot.toString();
	}

	public String getSuccessJsonResponse(String[] messages) {
		jsonObjectRoot.addProperty(RESPONSE, OK);
		JsonArray msgs = new JsonArray();
		for(String m : messages) {
			msgs.add(m);
		}
		jsonObjectRoot.add(MESSAGE, msgs);
		return jsonObjectRoot.toString();
	}
	
	
	public String getSuccessJsonResponse(String message) {
		return getResponse(true, message);
	}
	
	public String getErrorJsonResponse(String message) {
		return getResponse(false, message);
	}

	public String getSuccessJsonResponse(String language, String message) {
		return getResponse(true, language, message);
	}
	
	private String getResponse(boolean success, String message) {
		jsonObjectRoot.addProperty(RESPONSE, (success ? OK : ERROR));
		jsonObjectRoot.addProperty(MESSAGE, message);
		return jsonObjectRoot.toString();
	}

	private String getResponse(boolean success, String language, String message) {
		jsonObjectRoot.addProperty(RESPONSE, (success ? OK : ERROR));
		jsonObjectRoot.addProperty(LANGUAGE, language);
		jsonObjectRoot.addProperty(MESSAGE, message);
		return jsonObjectRoot.toString();
	}

/*	
	public void buildNerOutputJson(NerOutput nerOut) {
		// build the root json object initial property fields
		jsonObjectRoot.addProperty(TEXT, nerOut.getText());
		jsonObjectRoot.addProperty(ANNOTATIONS, nerOut.getAnnotations());
		jsonObjectRoot.addProperty(LANGUAGE, nerOut.getLanguage());

		// set the result array (main array)
		JsonArray jsonResults = new JsonArray();
		ArrayList<NerAlgorithmResult> results = nerOut.getResults();

		for (NerAlgorithmResult result : results) {
			JsonObject jsonResult = buildResult(result);
			jsonResults.add(jsonResult);
		}

		jsonObjectRoot.add(RESULT, jsonResults);
	}

	private JsonObject buildResult(NerAlgorithmResult result) {
		JsonObject jsonResult = new JsonObject();
		jsonResult.addProperty(ALGORITHM, result.getAlgorithmName());

		JsonObject jsonEntities = new JsonObject();
		ArrayList<NerEntity> entities = result.getEntities();
		if (entities != null)
			for (NerEntity entity : entities) {
				JsonArray jsonEntityValues = buildEntityValues(entity);
				jsonEntities.add(entity.getEntityName(), jsonEntityValues);
			}

		jsonResult.add(ENTITIES, jsonEntities);
		return jsonResult;
	}

	private JsonArray buildEntityValues(NerEntity entity) {
		// JsonObject jsonEntity = new JsonObject();
		JsonArray values = new JsonArray();
		ArrayList<NerEntityData> entityData = entity.getEntities();
		if (entityData != null)
			for (NerEntityData data : entityData) {
				JsonObject jsonEntityData = buildEntityData(data);
				values.add(jsonEntityData);
			}
		// jsonEntity.add(entity.getEntityName(), values);
		return values;
	}

	private JsonObject buildEntityData(NerEntityData data) {
		JsonObject entityData = new JsonObject();
		JsonArray indices = new JsonArray();
		indices.add(data.getStart());
		indices.add(data.getEnd());
		entityData.add(INDICES, indices);
		ArrayList<NameValue> fields = data.getAdditionalFields();
		if (fields != null)
			for (NameValue field : fields) {
				entityData.addProperty(field.getName(), field.getValue());
			}
		return entityData;
	}
	*/
}
