package org.gcube.data.analysis.nlphub.shared;

public class Constants {
	public static final boolean DEBUG = false;
	public static final boolean TEST_ENABLE = false;

	public static final String DEFAULT_USER = "giancarlo.panichi";
	public static final String DEFAULT_SCOPE = "/gcube/devNext/NextNext";
	public static final String DEFAULT_TOKEN = "";

	public static final String IS_URL = "http://registry.d4science.org";

	public static final String STORAGEHUB_SERVICE_CLASS = "DataAccess";
	public static final String STORAGEHUB_SERVICE_NAME = "StorageHub";

	public static final String DATA_MINER_SERVICE_NAME = "DataMiner";
	public static final String DATAMINER_SERVICE_CATEGORY = "DataAnalysis";

	public static final String DEFAULT_DATAMINER_URL = "http://dataminer-prototypes.d4science.org/wps/WebProcessingService";

	public static final String TOKEN_PARAMETER = "gcube-token";

	public static final String DEFAULT_DESCRIPTION = "NlpHub upload";

	public static final String MIME_TEXT = "text/plain";
	public static final String CONTENT_TYPE = "Content-Type";
	public static final String UNAVAILABLE = "unavailable";
	public static final String ERROR_ID = "ERROR";
	public static final String INPUT_FILE_PARAMETER = "input";
	public static final String REQUEST = "Request";

	/*
	 * public static String hexDump(byte[] bytes) { char[] hexArray =
	 * "0123456789ABCDEF".toCharArray(); char[] hexChars = new char[bytes.length
	 * * 3]; for (int j = 0; j < bytes.length; j++) { hexChars[3*j] =
	 * hexArray[bytes[j] / 16]; hexChars[3*j+1] = hexArray[bytes[j] % 16];
	 * hexChars[3*j+2] = ' '; } return new String(hexChars); }
	 */
}
