package org.gcube.data.analysis.nlphub.nlp;

import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.URL;
import java.net.URLEncoder;

import javax.servlet.http.HttpServletResponse;
import javax.xml.parsers.DocumentBuilderFactory;

import org.gcube.data.analysis.nlphub.is.DMDiscover;
import org.gcube.data.analysis.nlphub.legacy.JsonManager;
import org.gcube.data.analysis.nlphub.legacy.NlpHubException;
import org.gcube.data.analysis.nlphub.shared.Constants;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.w3c.dom.Document;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;
import org.xml.sax.InputSource;

public class NLpLanguageRecognizer {
	private static final Logger logger = LoggerFactory.getLogger(NLpLanguageRecognizer.class);

	// private HttpServletResponse response;
	// private String sentence, publicLink;
	public final static String RECOGNIZER_ID = "org.gcube.dataanalysis.wps.statisticalmanager.synchserver.mappedclasses.transducerers.LANGUAGE_RECOGNIZER";
	// private String dataMiner = null;

	public NLpLanguageRecognizer(String service, String token, String sentence) {
		logger.debug(
				"NLpLanguageRecognizer: [service=" + service + ", token=" + token + ", sentence=" + sentence + "]");
		// this.sentence = sentence;
		// response = null;
	}

	public NLpLanguageRecognizer(String dataMiner, String service, String token, String sentence, String publicLink,
			HttpServletResponse response) {
		logger.debug("NLpLanguageRecognizer: [dataMiner=" + dataMiner + ",service=" + service + ", token=" + token
				+ ", sentence=" + sentence + ",publicLink" + publicLink + "]");
		// this.sentence = sentence;
		// this.response = response;
		// this.publicLink = publicLink;
		// this.dataMiner = dataMiner;
	}

	public static void run(String dataMiner, String sentence, String token, String publicLink,
			HttpServletResponse response) throws NlpHubException {
		try {

			if (dataMiner == null || dataMiner.isEmpty()) {
				try {
					DMDiscover discoverDataMinerService = new DMDiscover();
					dataMiner = discoverDataMinerService.retrieveServiceUrl(token);
				} catch (Exception e) {
					logger.error("Error retrieving DataMiner service:" + e.getMessage(), e);
					throw new Exception("Error retrieving DataMiner service:" + e.getMessage());
				}
			}

			// else
			// dataMiner = "http://" + dataMiner;
			String urlService = dataMiner + "?request=Execute&service=WPS&Version=1.0.0";
			urlService += "&gcube-token=" + token;
			urlService += "&lang=en-US";
			urlService += "&Identifier=" + RECOGNIZER_ID;
			urlService += "&DataInputs=sentence=" + URLEncoder.encode(sentence, "UTF-8");
			URL url = new URL(urlService);
			HttpURLConnection connection = (HttpURLConnection) url.openConnection();
			// connection.setRequestProperty(Constants.TOKEN_PARAMETER,
			// super.getToken());
			connection.setDoInput(true);
			connection.setDoOutput(true);
			connection.setUseCaches(false);
			connection.setRequestMethod("GET");

			BufferedReader r = new BufferedReader(new InputStreamReader(connection.getInputStream()));
			Document doc = DocumentBuilderFactory.newInstance().newDocumentBuilder().parse(new InputSource(r));
			doc.getDocumentElement().normalize();
			NodeList nListData = doc.getElementsByTagName("d4science:Data");
			NodeList nListDesc = doc.getElementsByTagName("d4science:Description");

			int len = nListData.getLength();
			for (int i = 0; i < len; i++) {
				Node data = nListData.item(i);
				Node description = nListDesc.item(i);
				String link = data.getTextContent();
				String type = description.getTextContent();
				if (type.equals("outfile")) {
					// System.out.println(link);
					String content = readFileContent(link, token);
					if (response != null) {
						response.getWriter().println(new JsonManager().getSuccessJsonResponse(content, publicLink));
					} else {
						logger.debug(new JsonManager().getSuccessJsonResponse(content, publicLink));
					}
				}
			}

		} catch (Exception e) {
			logger.error(e.getLocalizedMessage(), e);
			throw new NlpHubException(e.getLocalizedMessage(), e);
		}

	}

	private static String readFileContent(String link, String token) throws Exception {
		URL url = new URL(link);
		HttpURLConnection connection = (HttpURLConnection) url.openConnection();
		connection.setRequestProperty(Constants.TOKEN_PARAMETER, token);
		connection.setDoInput(true);
		connection.setDoOutput(true);
		connection.setUseCaches(false);
		connection.setRequestMethod("GET");

		BufferedReader r = new BufferedReader(new InputStreamReader(connection.getInputStream()));
		StringBuffer response = new StringBuffer();
		String inputLine;
		while ((inputLine = r.readLine()) != null) {
			response.append(inputLine);
		}
		connection.disconnect();
		String out = response.toString();
		return out;
	}
	/*
	 * private String readFileContent(String link) throws Exception { URL url =
	 * new URL(link); HttpURLConnection connection = (HttpURLConnection)
	 * url.openConnection();
	 * connection.setRequestProperty(Constants.TOKEN_PARAMETER,
	 * super.getToken()); connection.setDoInput(true);
	 * connection.setDoOutput(true); connection.setUseCaches(false);
	 * connection.setRequestMethod("GET");
	 * 
	 * BufferedReader r = new BufferedReader(new
	 * InputStreamReader(connection.getInputStream())); StringBuffer response =
	 * new StringBuffer(); String inputLine; while ((inputLine = r.readLine())
	 * != null) { response.append(inputLine); } connection.disconnect(); String
	 * out = response.toString(); return out; }
	 */

}
