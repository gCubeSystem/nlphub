package org.gcube.data.analysis.nlphub;

import java.io.IOException;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.gcube.data.analysis.nlphub.session.SessionUtils;
import org.gcube.data.analysis.nlphub.shared.Constants;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * Servlet implementation class NLPHub
 */
@WebServlet(asyncSupported = true, name = "NLPInit", urlPatterns = { "/nlpinit-servlet" })
public class NLPInit extends HttpServlet {
	private static final long serialVersionUID = 1L;
	private static final Logger logger = LoggerFactory.getLogger(NLPInit.class);

	
	public NLPInit() {
		super();
	}

	@Override
	protected void doGet(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		doWork(request, response);
	}

	@Override
	protected void doPost(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		doWork(request, response);
	}

	private void doWork(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {

		logger.debug("NLPInit");
		SessionUtils sessionUtils=new SessionUtils();
		String token = sessionUtils.getToken(request);
		String res = request.getParameter(Constants.REQUEST);
		RequestType r=RequestType.getRequestTypeFromString(res);
		if(r==null){
			throw new ServletException("Invalid request: "+res);
		}
		switch(r){
		case DataminerService:
			dataminerServiceDiscover(request, response, token);			
			break;
		default:
			throw new ServletException("Invalid request: "+res);
			
		
		}
	}
	
	private void dataminerServiceDiscover(HttpServletRequest request, HttpServletResponse response,String token) throws ServletException {
		
		/*
		PrintWriter writer = response.getWriter();
		response.setContentType("text/plain;charset=UTF-8");
		response
		String result = null;
					result = request.getSession().getId();
	
		} catch (Exception e) {
			logger.error(e.getLocalizedMessage(), e);

		} finally {
			writer.println(result);
			writer.close();
		}*/

		
	}

	
}
