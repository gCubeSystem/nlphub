package org.gcube.data.analysis.nlphub.legacy;

import java.util.ArrayList;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.google.gson.JsonArray;
import com.google.gson.JsonObject;

public class NerOutput {
	private static final Logger logger = LoggerFactory.getLogger(NerOutput.class);

	private String text, annotations, language;
	private ArrayList<NerAlgorithm> result;

	public NerOutput(String text, String annotations, String language) {
		logger.debug("NerOutput: [text="+text+", annotations="+annotations+", language="+language+"]");
		this.text = text;
		this.annotations = annotations;
		this.language = language;
		result = new ArrayList<>();
	}
	
	public void addNerAlgorithm(NerAlgorithm alg) {
		result.add(alg);
	}

	public JsonObject toJson() {
		JsonObject json = new JsonObject();
		json.addProperty("text", text);
		json.addProperty("annotations", annotations);
		json.addProperty("language", language);
		
		JsonArray jResult = new JsonArray();
		for(NerAlgorithm a : result) {
			jResult.add(a.toJson());
		}
		
		json.add("result",jResult);
		return json;
	}
	
	public String getText() {
		return text;
	}

	public void setText(String text) {
		this.text = text;
	}

	public String getAnnotations() {
		return annotations;
	}

	public void setAnnotations(String annotations) {
		this.annotations = annotations;
	}

	public String getLanguage() {
		return language;
	}

	public void setLanguage(String language) {
		this.language = language;
	}

	public ArrayList<NerAlgorithm> getResult() {
		return result;
	}

	public void setResult(ArrayList<NerAlgorithm> result) {
		this.result = result;
	}
	
	public static void main(String[] args) {
		NerAlgorithm alg = new NerAlgorithm("Algorithm-name");
		
		NerEntity ne1 = new NerEntity(11, 22);
		ne1.addProperty("type", "scalar");
		ne1.addProperty("unit", "hours");

		NerEntity ne2 = new NerEntity(33, 44);
		ne2.addProperty("type", "scalar");
		ne2.addProperty("unit", "minutes");

		NerAnnotationData nad = new NerAnnotationData("Measure");
		nad.addNerEntity(ne1);
		nad.addNerEntity(ne2);
		
		alg.addAnnotationData(nad);

		ne1 = new NerEntity(0, 10);
		ne2 = new NerEntity(111, 114);
		nad = new NerAnnotationData("Person");
		nad.addNerEntity(ne1);
		nad.addNerEntity(ne2);
		
		alg.addAnnotationData(nad);

		NerAlgorithm alg2 = new NerAlgorithm("Algorithm-name-2");
		
		ne1 = new NerEntity(1, 22);
		ne2 = new NerEntity(30, 33);

		nad = new NerAnnotationData("Measure");
		nad.addNerEntity(ne1);
		nad.addNerEntity(ne2);
		
		alg2.addAnnotationData(nad);

		
		NerOutput no = new NerOutput("this is the text", "Measure,Person", "english");
		no.addNerAlgorithm(alg);
		no.addNerAlgorithm(alg2);
		
		System.out.println(no.toJson().toString());
		
	}

}
