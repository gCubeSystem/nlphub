package org.gcube.data.analysis.nlphub.legacy;

public class NlpHubException extends Exception {
	
	private static final long serialVersionUID = 1L;

	public NlpHubException(String message) {
		super(message);
	}
	public NlpHubException(String message, Throwable throwable) {
		super(message, throwable);
	}
}
