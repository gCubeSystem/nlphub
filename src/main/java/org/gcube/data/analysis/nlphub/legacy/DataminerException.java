package org.gcube.data.analysis.nlphub.legacy;


class DataminerClientException extends Exception {

	private static final long serialVersionUID = 1L;

	public DataminerClientException(String message, Throwable throwable) {
		super(message, throwable);
	}
};