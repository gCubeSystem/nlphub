package org.gcube.data.analysis.nlphub.legacy;

import java.util.ArrayList;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.google.gson.JsonArray;
import com.google.gson.JsonObject;

public class NerAlgorithm {
	private static final Logger logger = LoggerFactory.getLogger(NerAlgorithm.class);

	private String name;
	private ArrayList<NerAnnotationData> annotationsData;

	public NerAlgorithm(String name) {
		logger.debug("NerAlgorithm: "+name);
		
		this.name = name;
		annotationsData = new ArrayList<>();
	}

	public void addAnnotationData(NerAnnotationData data) {
		annotationsData.add(data);
	}

	public JsonObject toJson() {
		JsonObject json = new JsonObject();
		json.addProperty("algorithm", name);
		JsonArray data = new JsonArray();
		for (NerAnnotationData d : annotationsData) {
			data.add(d.toJson());
		}
		json.add("entities", data);
		return json;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public ArrayList<NerAnnotationData> getAnnotationsData() {
		return annotationsData;
	}

	public void setAnnotationsData(ArrayList<NerAnnotationData> annotationsData) {
		this.annotationsData = annotationsData;
	}

	/*
	public static void main(String[] args) {
		NerAlgorithm alg = new NerAlgorithm("Algorithm-name");
		
		NerEntity ne1 = new NerEntity(11, 22);
		ne1.addProperty("type", "scalar");
		ne1.addProperty("unit", "hours");

		NerEntity ne2 = new NerEntity(33, 44);
		ne2.addProperty("type", "scalar");
		ne2.addProperty("unit", "minutes");

		NerAnnotationData nad = new NerAnnotationData("Measure");
		nad.addNerEntity(ne1);
		nad.addNerEntity(ne2);
		
		alg.addAnnotationData(nad);

		ne1 = new NerEntity(0, 10);
		ne2 = new NerEntity(111, 114);
		nad = new NerAnnotationData("Person");
		nad.addNerEntity(ne1);
		nad.addNerEntity(ne2);
		
		alg.addAnnotationData(nad);
		

		System.out.println(alg.toJson().toString());
	}*/
}
